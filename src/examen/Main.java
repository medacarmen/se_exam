package examen;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Main {

    public static Thread CThread1;
    public static Thread CThread2;
    public static ScheduledExecutorService scheduler;

    public static void main(String[] args) {

        CThread1 = new Thread(new ThreadMessage("CThread1"));
        CThread2 = new Thread(new ThreadMessage("CThread2"));

        scheduler = Executors.newScheduledThreadPool(2);

        scheduler.scheduleAtFixedRate(CThread1, 0, 2, TimeUnit.SECONDS);
        scheduler.scheduleAtFixedRate(CThread2, 0, 2, TimeUnit.SECONDS);
    }


    static class ThreadMessage implements Runnable
    {
        private String numeThread;
        private int nrAfisari;

        public ThreadMessage(String numeThread)
        {
            this.numeThread = numeThread;
            this.nrAfisari = 0;
        }

        public void run()
        {

            try
            {
                if (nrAfisari == 7)
                {
                    scheduler.shutdown();
                }
                else
                {
                    nrAfisari += 1;
                    String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
                 //   var date = new Date();
                   System.out.println(numeThread + " - " + timeStamp);
                }
            }
            catch (Exception e)
            {
                System.out.println ("Exception is caught ThreadMessage!");
            }
        }
    }
}